class CreateEstimatedSales < ActiveRecord::Migration[5.1]
  def change
    create_table :estimated_sales do |t|
      t.string :country
      t.string :category
      t.integer :rank
      t.integer :sales_count

      t.timestamps
    end
  end
end
