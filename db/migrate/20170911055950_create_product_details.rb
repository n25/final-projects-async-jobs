class CreateProductDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :product_details do |t|
      t.string :asin
      t.string :country
      t.string :category
      t.integer :rank

      t.timestamps
    end
  end
end
