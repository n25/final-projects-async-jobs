# README

## Goal of the example

I want to show how to use Sidekiq to do background processing. To do that I'll use 3 models:

* `Product`,
* `ProductDetail`, and
* `EstimatedSale`.

The user start by creating a `Product` and the creation of this product must _trigger_ jobs
that will create multiple `ProductDetail` and *then* the associated `EstimatedSale`.

## How I get to this point

* Create the directory that will hold the Rails app

    mkdir final-projects-async-jobs

* Install required gems

    gem install rails

* Generate an empty application

    rails new . --skip-spring --skip-turbolinks --skip-action-mailer --database=sqlite3

* Add the `gem 'sidekiq'` to the `Gemfile` and then run

    bundle install

* In order to make sidekiq work, install and launch Redis (for mac OS, read [this](redis-osx))

    sudo apt-get install redis-server # For deb-based linux only

* Make sure Redis is running by opening a client and closing it

    redis-cli
    <CTRL-D>

* Add the Sidekiq Web UI in the `config/routes.rb`

``` ruby
require "sidekiq/web"

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
end
```

* Start your Rails application and visit `localhost:3000/sidekiq`

    bundle exec rails server

* Generate dummy models to work with

    bundle exec rails generate model product asin:string state:string
    bundle exec rails generate model product_detail asin:string country:string category:string rank:integer
    bundle exec rails generate model estimated_sale country:string category:string rank:integer sales_count:integer

* Run the migration to actually create the tables

    bundle exec rake db:migrate

* Create `ProductCreate` and `ProductDetailCreate` services wrapping the creation of some models

* Create `FindNicheWorker` and `FindEstimatedSaleWorker` with

    bundle exec rails generate sidekiq:worker FindNiche
    bundle exec rails generate sidekiq:worker FindEstimatedSale

## Play with it

In a `bundle exec rails console`, run

``` ruby
ProductCreate.new(asin: "1234").perform
```

In another terminal start Rails with `bundle exec rails server` and visit localhost:3000/sidekiq/queues/default
You should see the jobs present in the _queue_ named `default`.

At this point the job isn't processed and won't be because there is no _workers_ running.
You can check that here: localhost:3000/sidekiq/busy

In the meantime, you can check that there is no ProductDetail in the database in the console

``` ruby
ProductDetail.count # => 0
```

To get the processing done, You'll need to start some workers with the following command in a new terminal

    bundle exec sidekiq

In that terminal you'll see:

    2017-09-11T06:52:10.101Z 28503 TID-grssvvbo4 INFO: Running in ruby 2.4.0p0 (2016-12-24 revision 57164) [x86_64-linux]
    2017-09-11T06:52:10.102Z 28503 TID-grssvvbo4 INFO: See LICENSE and the LGPL-3.0 for licensing details.
    2017-09-11T06:52:10.102Z 28503 TID-grssvvbo4 INFO: Upgrade to Sidekiq Pro for more features and support: http://sidekiq.org
    2017-09-11T06:52:10.102Z 28503 TID-grssvvbo4 INFO: Booting Sidekiq 5.0.4 with redis options {:id=>"Sidekiq-server-PID-28503", :url=>nil}
    2017-09-11T06:52:10.103Z 28503 TID-grssvvbo4 INFO: Starting processing, hit Ctrl-C to stop
    2017-09-11T06:52:10.118Z 28503 TID-grst8zrdw FindNicheWorker JID-8507ec74a67bf53a62ec0b1f INFO: start
    2017-09-11T06:52:15.371Z 28503 TID-grst8zqwo FindEstimatedSaleWorker JID-cd8bd84d63e2c0db85b3c66e INFO: start
    2017-09-11T06:52:20.416Z 28503 TID-grst95lfk FindEstimatedSaleWorker JID-ec3280556e8beca8afa3148e INFO: start
    2017-09-11T06:52:21.416Z 28503 TID-grst8zqwo FindEstimatedSaleWorker JID-cd8bd84d63e2c0db85b3c66e INFO: done: 6.046 sec
    2017-09-11T06:52:21.441Z 28503 TID-grst95lfk FindEstimatedSaleWorker JID-ec3280556e8beca8afa3148e INFO: done: 1.025 sec
    2017-09-11T06:52:25.442Z 28503 TID-grst8zqwo FindEstimatedSaleWorker JID-5c758f50938bba17db6e9d67 INFO: start
    2017-09-11T06:52:26.470Z 28503 TID-grst8zqwo FindEstimatedSaleWorker JID-5c758f50938bba17db6e9d67 INFO: done: 1.028 sec
    2017-09-11T06:52:30.463Z 28503 TID-grst8zqwo FindEstimatedSaleWorker JID-bddf9eb1ed4999703b0e04c7 INFO: start
    2017-09-11T06:52:31.483Z 28503 TID-grst8zqwo FindEstimatedSaleWorker JID-bddf9eb1ed4999703b0e04c7 INFO: done: 1.02 sec
    2017-09-11T06:52:35.488Z 28503 TID-grst8zq9w FindEstimatedSaleWorker JID-7218bb5bf717cda1726a2583 INFO: start
    2017-09-11T06:52:36.527Z 28503 TID-grst8zq9w FindEstimatedSaleWorker JID-7218bb5bf717cda1726a2583 INFO: done: 1.039 sec
    2017-09-11T06:52:40.519Z 28503 TID-grst8zq9w FindEstimatedSaleWorker JID-c2bc67b27f901513d6aa101d INFO: start
    2017-09-11T06:52:41.550Z 28503 TID-grst8zq9w FindEstimatedSaleWorker JID-c2bc67b27f901513d6aa101d INFO: done: 1.031 sec
    2017-09-11T06:52:45.546Z 28503 TID-grst8zo1c FindEstimatedSaleWorker JID-17e8a92500b2fac00afea39f INFO: start
    2017-09-11T06:52:46.570Z 28503 TID-grst8zo1c FindEstimatedSaleWorker JID-17e8a92500b2fac00afea39f INFO: done: 1.024 sec
    2017-09-11T06:52:50.583Z 28503 TID-grst95lfk FindEstimatedSaleWorker JID-1990a49fd44b0e46bdc99783 INFO: start
    2017-09-11T06:52:51.612Z 28503 TID-grst95lfk FindEstimatedSaleWorker JID-1990a49fd44b0e46bdc99783 INFO: done: 1.029 sec
    2017-09-11T06:52:55.623Z 28503 TID-grst8zr3w FindEstimatedSaleWorker JID-822659898296bea8f6ebbf71 INFO: start
    2017-09-11T06:52:56.647Z 28503 TID-grst8zr3w FindEstimatedSaleWorker JID-822659898296bea8f6ebbf71 INFO: done: 1.024 sec
    2017-09-11T06:53:00.658Z 28503 TID-grst95jfc FindEstimatedSaleWorker JID-4aaf2b46a6467af4a534bff5 INFO: start
    2017-09-11T06:53:00.661Z 28503 TID-grst8zrdw FindNicheWorker JID-8507ec74a67bf53a62ec0b1f INFO: done: 50.543 sec
    2017-09-11T06:53:01.688Z 28503 TID-grst95jfc FindEstimatedSaleWorker JID-4aaf2b46a6467af4a534bff5 INFO: done: 1.03 sec

These are the job running. After that, checking the count for `ProductDetail` and `EstimatedSale` should give you `10`.

## Conclusion

Having the Rails app running + sidekiq would allow you to do scrapping in the backgroud in parallel. You can execute many jobs at
the same time using `bundle exec sidekiq -c 10` which will be able to work 10 jobs at the same time.

Instead of scrapping sequencially on amazon page then another, you can trigger a job for each page. Those jobs may be able to run concurrently
and get you much faster results.

[redis-osx]: https://medium.com/@petehouston/install-and-config-redis-on-mac-os-x-via-homebrew-eb8df9a4f298
