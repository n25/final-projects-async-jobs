class ProductDetailCreate
  def initialize(*args)
    @args = args
  end

  def perform
    ProductDetail.create!(*@args).tap do |new_product_detail|
      # After creating a product detail, Ask for its estimated sales to the FindEstimatedSaleWorker
      # This will enqueue a job in sidekiq.
      FindEstimatedSaleWorker.perform_async(new_product_detail.id)
    end
  end
end
