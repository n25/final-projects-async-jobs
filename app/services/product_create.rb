class ProductCreate
  def initialize(*args)
    @args = args
  end

  def perform
    Product.create!(*@args).tap do |new_product|
      # After creating a product, Ask for it niche to the FindNicheWorker
      # This will enqueue a job in sidekiq.
      FindNicheWorker.perform_async(new_product.id)
    end
  end
end
