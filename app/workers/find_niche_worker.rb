class FindNicheWorker
  include Sidekiq::Worker

  def perform(product_id)
    product = Product.find(product_id)

    # Create 10 product details based on this product which will be the niche
    10.times do |i|
      create_random_product_detail(product, i)
    end
  end

  private

  def create_random_product_detail(product, index)
    # Use Sonar or anything to get there
    sleep 5 # Fake some latency for the example
    ProductDetailCreate.new({
      asin: "#{product.asin}-#{index}",
      country: "fr",
      category: "Animalerie",
      rank: index,
    }).perform
  end
end
