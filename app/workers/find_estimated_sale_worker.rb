class FindEstimatedSaleWorker
  include Sidekiq::Worker

  def perform(product_detail_id)
    product_detail = ProductDetail.find(product_detail_id)

    EstimatedSale.create!({
      country: product_detail.country,
      category: product_detail.category,
      rank: product_detail.rank,
      sales_count: find_estimated_sale(product_detail),
    })
  end

  private

  def find_estimated_sale(product_detail)
    # Use Jungle Scout scrapper instead of returning that fake value
    sleep 1 # To fake some delay getting the result from JS
    42
  end
end
